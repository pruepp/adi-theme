The WordPress theme for [Die Autodidaktische Initiative](http://adi-leipzig.net).

A child theme of the official TwentyFourteen theme. Compatible with at least v1.7.

GPL2+
