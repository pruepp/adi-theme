
<div class="adi_frontseite">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
			// Page thumbnail and title.
			twentyfourteen_post_thumbnail();
			//the_title( '<header class="entry-header"><h1 class="entry-title">', '</h1></header><!-- .entry-header -->' );
		?>

		<div class="entry-content">
			<?php
				the_content();

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );

				edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
			?>
		</div><!-- .entry-content -->
	</article><!-- #post-## -->


	<div id="adi_news">
		<?php

		// Ankündigungen should be set as the default category!
		$args = array(
			'posts_per_page'   => 2,
			'category'         => get_option( 'default_category' ),
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'post_type'        => 'post',
			'post_status'      => 'publish',
			'suppress_filters' => true );

		$posts_array = get_posts( $args );

		if ( ! empty( $posts_array ) ) {
			$post = $posts_array[0];

			foreach ( $posts_array as $post ) : setup_postdata( $post ); ?>

				<article class="post type-post status-publish format-standard hentry adi_frontpage_news">

					<header class="entry-header">
						<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content();?>
					</div><!-- .entry-content -->

				</article>

			<?php endforeach; 

			wp_reset_postdata();

			echo '<article><div class="entry-content"><a href="http://adi-leipzig.net/?cat=' . get_option( 'default_category' ) . '">Alle Ankündigungen</a></div></article>';
		}
		?>
	</div>
</div>
